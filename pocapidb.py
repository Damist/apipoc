from flask import Flask, request, jsonify, render_template
from flask_mysqldb import MySQL
import json
from flask_paginate import Pagination, get_page_parameter
import hashlib
import mttqpublis

app = Flask(__name__)
app.config['MYSQL_HOST'] = '34.101.251.89'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'M4r14_M3rc3d35!'
app.config['MYSQL_DB'] = 'GEM'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor' # line of code you require

mysql = MySQL(app)
def failedRequest(requestMessage):
    return jsonify({'responsecode': '500',
                    'message': requestMessage})

def CheckIfLoginExist(email):
   cur = mysql.connection.cursor()
   print(email)
   sqlquery='SELECT COUNT(Email) as jumlah FROM  `User` u where Email=%s'
   cur.execute(sqlquery,(email,))
   rv=cur.fetchall()
   for i in rv:
      if i['jumlah']>=1:
         return True
   return False
def findUserRole(email):
   cur = mysql.connection.cursor()
   print(email)
   sqlquery='select  ur.Roleid,u.username from `User` u  left join UserRole ur on ur.UserId =u.UserId where u.Email=%s'
   cur.execute(sqlquery,(email,))
   rv=cur.fetchall()
   print(rv)
   if len(rv)>=1:
      return rv
   return ''
@app.route('/hello')
def index():
   cur = mysql.connection.cursor()
   cur.execute("SELECT * FROM Users WHERE id=1")
   rv = cur.fetchall()
   return jsonify(rv) # use jsonify here

@app.route('/login',methods=['POST'])
def login():
   userlogin = json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery="SELECT COUNT(Email) as jumlah FROM  `User` u where Email=%s and Password=%s"
   hash_object = hashlib.sha256(userlogin['Password'].encode())
   hashedpassword = hash_object.hexdigest()
   print(hashedpassword)
   cur.execute(sqlquery,(userlogin['Email'],hashedpassword))
   rv=cur.fetchall()
   print(rv)
   role=findUserRole(userlogin['Email'])
   print(role)
   for i in rv:
      if i['jumlah']>=1:
         return jsonify({'responsecode': '200',
                        'message': 'login success','Roleid':role[0]['Roleid'],'UserName':role[0]['username']})
   return failedRequest('email and password not found') # use jsonify here

@app.route('/getAllUser',methods=['GET'])
def getAllUser():
   cur = mysql.connection.cursor()
   sqlquery='SELECT  * FROM  GetAllUserRole '
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'users':rv,'responsecode': '200',
                        'message': 'success get all user'}) # use jsonify here

@app.route('/getAllTruck',methods=['GET'])
def getAllTruck():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Truck'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'trucks':rv,'responsecode': '200',
                        'message': 'success get all truck'}) # use jsonify here

@app.route('/getAllRom',methods=['GET'])
def getAllRom():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Rom'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'roms':rv,'responsecode': '200',
                        'message': 'success get all roms'}) # use jsonify here

@app.route('/getAllPort',methods=['GET'])
def getAllPort():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Port'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'ports':rv,'responsecode': '200',
                        'message': 'success get all port'}) # use jsonify here



@app.route('/getAllUserRole',methods=['GET'])
def getAllUserRole():
   cur = mysql.connection.cursor()
   sqlquery='SELECT  * FROM  UserRole'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'userroles':rv,'responsecode': '200',
                        'message': 'success get all userrole'})

@app.route('/getAllCheckIn',methods=['GET'])
def getAllCheckin():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Checkin'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'checkins':rv,'responsecode': '200',
                        'message': 'success get all checkin '}) # use jsonify here

@app.route('/getAllAssignmentmatching',methods=['GET'])
def getAllAssignmentmatching():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.AssignmentMatching'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'trucks':rv,'responsecode': '200',
                        'message': 'success get all truck'}) # use jsonify here

@app.route('/getAllAssignment',methods=['GET'])
def getAllAssignment():
   cur = mysql.connection.cursor()
   sqlquery='select a.AssignmentId,a.BussinesDate,a.BusinessShift,a.ROMId,r.ROMName,a.PortId ,p.PortName ,a.TruckLat ,a.TruckLng ,a.DriverAcceptTime ,a.DepartureTimeFromPort ,a.DeliverHour ,a.EstimatedArrivalTime ,a.ArrivalTime ,a.NetWeightArrival  ,a.`Point` ,a.Status ,a.Deskripsi ,a.RequesterId ,u.UserName,a.DepatureTimeFromROM from GEM.`Assignment` a left join GEM.ROM r on r.ROMId = a.ROMId LEFT  join GEM.Port p on p.PortId = a.PortId left join GEM.`User` u on u.UserId = a.RequesterId'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'assignments':rv,'responsecode': '200',
                        'message': 'success get all assignments'}) # use jsonify here

@app.route('/getAllAssignment2',methods=['GET'])
def getAllAssignment2():
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.GetAllAssignment'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   print(rv)
   return jsonify({'assignments':rv,'responsecode': '200',
                        'message': 'success get all assignments'}) # use jsonify here

@app.route('/getAssignmentByid2',methods=['GET'])
def getAssignmentByid2():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.GetAllAssignment where assignmentid=%s'
   cur.execute(sqlquery,(assignment['AssignmentId'],))  
   rv=cur.fetchall()
   print(rv)
   return jsonify({'assignments':rv,'responsecode': '200',
                        'message': 'success get all assignments'}) # use jsonify here

@app.route('/getUserById',methods=['POST'])
def getUserByid():
   user=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT  * FROM  GetAllUserRole  UserId=%s'
   cur.execute(sqlquery,(user['UserId'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get user'}) # use jsonify here


@app.route('/getTruckById',methods=['POST'])
def getTruckment():
   truck=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Truck where TruckId=%s'
   cur.execute(sqlquery,(truck['TruckId'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get truck'}) # use jsonify here

@app.route('/getPortById',methods=['POST'])
def getPort():
   port=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Port where PortId=%s'
   cur.execute(sqlquery,(port['PortId'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get Port'}) # use jsonify here

@app.route('/getRomById',methods=['POST'])
def getRomById():
   rom=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.Rom where RomId=%s'
   cur.execute(sqlquery,(rom['TruckId'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get rom'}) # use jsonify here

@app.route('/getAssigmentById',methods=['POST'])
def getAssigmentByid():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='select a.AssignmentId,a.BussinesDate,a.BusinessShift,a.ROMId,r.ROMName,a.PortId ,p.PortName ,a.TruckLat ,a.TruckLng ,a.DriverAcceptTime ,a.DepartureTimeFromPort ,a.DeliverHour ,a.EstimatedArrivalTime ,a.ArrivalTime ,a.NetWeightArrival  ,a.`Point` ,a.Status ,a.Deskripsi ,a.RequesterId ,u.UserName,a.DepatureTimeFromROM from GEM.`Assignment` a left join GEM.ROM r on r.ROMId = a.ROMId LEFT  join GEM.Port p on p.PortId = a.PortId left join GEM.`User` u on u.UserId = a.RequesterId where a.AssignmentId=%s'
   cur.execute(sqlquery,(assignment['AssignmentId'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get Assigment'}) # use jsonify here                      

@app.route('/getAssignmentMatchingByid',methods=['POST'])
def getAssignmentMatchingByid():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='SELECT * FROM GEM.AssignmentMatching where AssignmentMatchingId=%s'
   cur.execute(sqlquery,(assignment['AssignmentMatching'],))
   rv=cur.fetchall()
   print(rv)
   return jsonify({'user':rv,'responsecode': '200',
                        'message': 'success get AssignmentMatching'}) # use jsonify here                      



@app.route('/testPagination')
def testPaging():
   page = request.args.get(get_page_parameter(), type=int, default=1)
   cur = mysql.connection.cursor()
   sqlquery="SELECT UserId, UserName, Password, CreateBy, CreateDate, UpdateBy, UpdateDate FROM GEM.`User` ORDER BY UserId"
   cur.execute(sqlquery)
   data=cur.fetchall
   return render_template('users/index.html',
                           users=data,
                           pagination=page,
                           )

@app.route('/deleteUserById',methods=['POST'])
def deleteUserByid():
   user=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.`User` where UserId=%s'
   cur.execute(sqlquery,(user['UserId'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete user'}) # use jsonify here
                        
@app.route('/deletePortById',methods=['POST'])
def deletePortById():
   port=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.Port where PortId=%s'
   cur.execute(sqlquery,(port['PortId'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete Port'}) # use jsonify here
@app.route('/deleteRomById',methods=['POST'])
def deleteRomByid():
   rom=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.Port where RomId=%s'
   cur.execute(sqlquery,(rom['RomId'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete Rom'}) # use jsonify here

@app.route('/deleteAssigmentById',methods=['POST'])
def deleteAssigm():
   assigment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.`Assignment` where AssignmentId=%s'
   cur.execute(sqlquery,(assigment['AssignmentId'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete Assignment'}) # use jsonify here

@app.route('/deleteAssigmentMatchingById',methods=['POST'])
def deleteAssigmentMatchingbyId():
   assigment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.AssignmentMatching where AssignmentMatchingId=%s'
   cur.execute(sqlquery,(assigment['AssignmentMatchingId'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete AssignmentMatchingId'}) # use jsonify here

@app.route('/deleteUserRoleById',methods=['POST'])
def deleteUserRoleById():
   user=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='DELETE FROM GEM.UserRole where UserRoleId=%s'
   cur.execute(sqlquery,(user['UserRoleid'],))
   return jsonify({'responsecode': '200',
                        'message': 'success delete UserRoleId'}) # use jsonify here  


@app.route('/insertUser',methods=['POST'])
def insertUser():
   user=json.loads(request.data)
   cur = mysql.connection.cursor()
   isExist=CheckIfLoginExist(user['Email'],)
   if isExist==False:
      sqlquery='INSERT INTO GEM.`User`(UserName, Password, CreateBy, CreateDate, UpdateBy, UpdateDate, Phone, Email,IsDeleted) VALUES(%s,%s,%s,current_timestamp(),NULL,NULL,%s,%s,%s)'
      hash_object = hashlib.sha256(user['Password'].encode())
      hashedpassword = hash_object.hexdigest()
      print(hashedpassword)
      cur.execute(sqlquery,(user['UserName'], hashedpassword, 'Admin',user['Phone'],user['Email'],user['IsDeleted']))
      mysql.connection.commit()
      rowid=cur.lastrowid
      return jsonify({'responsecode': '200',
                        'message': 'success insert user','UserId':rowid}) # use jsonify here
   elif isExist==True:
      return failedRequest('User is Exist')

@app.route('/insertPort',methods=['POST'])
def insertPort():
   port=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.Port(PortName, PortLat, PortLng, CreateBy, CreateDate, UpdateBy, UpdateDate)VALUES(%s, %s, %s, %s, current_timestamp(), NULL, NULL)'
   cur.execute(sqlquery,(port['PortName'], port['PortLat'], port['PortLng'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success insert port'}) # use jsonify here

@app.route('/insertRom',methods=['POST'])
def insertRom():
   rom=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.ROM(ROMName, ROMLat, ROMLng, CreateBy, CreateDate, UpdateBy, UpdateDate)VALUES(%s,%s, %s,%s, current_timestamp(), NULL, NULL)'
   cur.execute(sqlquery,(rom['ROMName'], rom['ROMLat'], rom['ROMLng'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success insert rom'}) # use jsonify here

@app.route('/insertTruck',methods=['POST'])
def insertTruck():
   truck=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.Truck(CallSign, `Type`, HaulingCompany, DriverId, CreateBy, CreateDate, UpdateBy, UpdateDate)VALUES(%s,%s,%s,%s,%s, current_timestamp(), NULL, NULL);'
   cur.execute(sqlquery,(truck['CallSign'], truck['Type'], truck['HaulingCompany'],truck['DriverId'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success insert truck'}) # use jsonify here
                               

@app.route('/insertAssignment',methods=['POST'])
def insertAssignment():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.`Assignment`(BussinesDate, BusinessShift, ROMId, PortId, TruckLat, TruckLng, DriverAcceptTime, DepatureTimeFromROM, DeliverHour, EstimatedArrivalTime, ArrivalTime, NetWeightArrival, DepartureTimeFromPort, `Point`, Status, Deskripsi, RequesterId, CheckInId, CreateBy, CreateDate, UpdateBy, UpdateDate)VALUES( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s,%s,%s, current_timestamp(), NULL, NULL)'
   cur.execute(sqlquery,(assignment['BussinesDate'], assignment['BusinessShift'], assignment['ROMId'],assignment['PortId'],assignment['TruckLat'],assignment['TruckLng'],assignment['DriverAcceptTime'],assignment['DepatureTimeFromROM'],assignment['DeliverHour'],assignment['EstimatedArrivalTime'],assignment['ArrivalTime'],assignment['NetWeightArrival'],assignment['DepartureTimeFromPort'],assignment['Point'],assignment['Status'],assignment['Deskripsi'],assignment['RequesterId'],assignment['CheckInId'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success insert assignment'}) # use jsonify here

@app.route('/insertAssignmentMatching',methods=['POST'])
def insertAssignmentMatching():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.AssignmentMatching(AssignmentId, CheckInId, Status, CreateBy, CreateDate, UpdateBy, UpdateDate, Reason)VALUES(%s, %s, %s, %s, current_timestamp(), NULL, NULL, NULL)'
   cur.execute(sqlquery,(assignment['AssignmentId'], assignment['CheckInId'], assignment['Status'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200', 
                        'message': 'success insert assigment'}) # use jsonify here



@app.route('/insertCheckin',methods=['POST'])
def insertCheckin():
   assignment=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='INSERT INTO GEM.CheckIn(BusinessDate, BusinessShift, DriverId, TruckId, CheckInDateTime, CheckOutDateTime, `Point`, Status, Lat, Lng, RitCount, CreateBy, CreateDate, UpdateBy, UpdateDate)VALUES(%s,%s, %s,  %s, current_timestamp(), %s, %s, %s, %s, %s, %s, %s, current_timestamp(), NULL, NULL)'
   cur.execute(sqlquery,(assignment['BusinessDate'], assignment['BusinessShift'], assignment['DriverId'],assignment['TruckId'],assignment['CheckOutDateTime'],assignment['Point'],assignment['Status'],assignment['Point'],assignment['Lat'],assignment['Lng'],assignment['RitCount'],'Admin'))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success insert chcekin'}) # use jsonify here



@app.route('/updateSystemMaster',methods=['POST'])
def updateUser():
   user=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='UPDATE GEM.SystemMaster SET Value=%s, CreateBy=%s, CreateDate=current_timestamp(), UpdateBy=NULL, UpdateDate=NULL WHERE Category=%s AND SubCategory=%s AND Code=%s'
   cur.execute(sqlquery,(user['Value'],'Admin',user['Category'],user['SubCategory'],user['Code']))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success Update SystemMaster'}) # use jsonify here


@app.route('/findDriver',methods=['Get'])
def findDriver():
   cur = mysql.connection.cursor()
   sqlquery='SELECT BusinessDate ,BusinessShift ,RitCount ,`Point` , 294 as radius, u.UserName ,t.HaulingCompany ,t.CallSign FROM CheckIn ci left join `User` u on ci.DriverId =u.UserId LEFT JOIN Truck t on ci.TruckId =t.TruckId ORDER by ci.`Point` DESC'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   return jsonify({'drivers':rv,'responsecode': '200',
                        'message': 'success find driver'}) # use jsonify here

@app.route('/sendNotifToDriver',methods=['POST'])
def sendNotifToDriver():
   cur = mysql.connection.cursor()
   sqlquery='SELECT r.ROMId ,r.ROMName ,r.ROMLat ,r.ROMLng , p.PortId ,p.PortName ,p.PortLat ,p.PortLng , a.EstimatedArrivalTime ,a.ArrivalTime ,a.Deskripsi FROM `Assignment` a left join ROM r on a.ROMId =r.ROMId LEFT JOIN Port p on a.PortId =p.PortId'
   cur.execute(sqlquery)
   rv=cur.fetchall()
   stringmessage=json.dumps(rv)
   print(stringmessage)
   mttqpublis.run(stringmessage)
   
@app.route('/updateAssignment',methods=['POST'])
def updateAssignment():
   assignmentNew=json.loads(request.data)
   cur = mysql.connection.cursor()
   sqlquery='update `Assignment` set DeliverHour = %s ,DepatureTimeFromROM =%s ,ArrivalTime =%s , DepartureTimeFromPort =%s ,NetWeightArrival =%s WHERE AssignmentId =%s'
   cur.execute(sqlquery,(assignmentNew['DeliverHour'],assignmentNew['Departurefromrom'],assignmentNew['ArrivalTime'],assignmentNew['Departurefromport'],assignmentNew['Netweightarrival'],assignmentNew['AssignmentID']))
   mysql.connection.commit()
   return jsonify({'responsecode': '200',
                        'message': 'success Update Assignment'}) # use jsonify here

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=107)