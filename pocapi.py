import json

from paho import mqtt
import mttqpublis
from flask import Flask, request, jsonify
app = Flask(__name__)

ports=[{'port_id': '001','port_name':'banjarbaru','longitude':'200', 'latitude':'300'}]
newPort={'port_id': '001','port_name':'sukalama','longitude':'200', 'latitude':'300'}
roms=[{'rom_id': '001','rom_name':'rom-001','longitude':'200', 'latitude':'300'}]
trucks=[{'truckid':'truck001','callsign':'cl-001','brand':'jl001','haulingcompany':'haul001','driverid':'driver001','max_rit':'299','status':'ok'}]
users=[{'userid':'test','username':'test','password':'test','createby':'test','createdate': 'test'}]
drivers=[{'userrolesid':'roletest','userid':'testid','email':'patrick@hosttest.com','password':'test123','rolesid':'test'}]
ports.append(newPort)

def failedRequest(requestMessage):
    return jsonify({'requestcode': '500',
                    'message': requestMessage})
   

@app.route('/hello/', methods=['GET', 'POST'])
def welcome():
    return "Hello World!"

@app.route('/login/', methods=['POST'])
def login():
    userLogin = json.loads(request.data)
    print(userLogin)
    for u in users:
        if userLogin['username'] ==u['username'] and userLogin['password']==u['password']:
            return jsonify({'requestcode': '200',
                        'message': 'login success'})
    return failedRequest('userNamePasswordNotFound')

@app.route('/getAllPort/', methods=['GET'])
def getAllPort():
    print(ports)
    if  len(ports)!=0:
        response=jsonify({'ports': ports,
                    'message': 'success return port','responsecode':'200'})
        return  response
    return failedRequest('port is empty')
    
@app.route('/getAllRom/', methods=['GET'])
def getAllRom():
    print(roms)
    if  len(roms)!=0:
        response=jsonify({'roms': roms,
                    'message': 'success return rom','responsecode':'200'})
        return  response
    return failedRequest('roms is empty')

@app.route('/getAllTrucks/', methods=['GET'])
def getAlltrucks():
    print(trucks)
    if  len(trucks)!=0:
        response=jsonify({'trucks': trucks,
                    'message': 'success return trucks','responsecode':'200'})
        return  response
    return failedRequest('truck is empty')

@app.route('/getAllUser/', methods=['GET'])
def getAlluser():
    print(users)
    if  len(users)!=0:
        response=jsonify({'users': users,
                    'message': 'success return users','responsecode':'200'})
        return  response
    return failedRequest('truck is empty')

@app.route('/getAllDrivers/', methods=['GET'])
def getAllDrivers():
    print(drivers)
    if  len(drivers)!=0:
        response=jsonify({'trucks': drivers,
                    'message': 'success return drivers','responsecode':'200'})
        return  response
    return failedRequest('drivers is empty')

@app.route('/addUser/', methods=['POST'])
def addUser():
    user = json.loads(request.data)
    print(user)
    users.append(user)
    print(users)
    response=jsonify({'message': 'success adding user','responsecode':'200'})
    return  response

@app.route('/addTruck/', methods=['POST'])
def addTruck():
    truck = json.loads(request.data)
    print(truck)
    trucks.append(truck)
    print(truck)
    response=jsonify({'message': 'success adding truck','responsecode':'200'})
    return  response

@app.route('/addDriver/', methods=['POST'])
def addDriver():
    driver = json.loads(request.data)
    print(driver)
    driver.append(driver)
    print(driver)
    response=jsonify({'message': 'success adding driver','responsecode':'200'})
    return  response

@app.route('/addPorts/', methods=['POST'])
def addPort():
    port = json.loads(request.data)
    print(port)
    ports.append(port)
    print(ports)
    response=jsonify({'message': 'success adding port','responsecode':'200'})
    return  response

@app.route('/getUserById/', methods=['POST'])
def getUserById():
    user = json.loads(request.data)
    print(user)
    for u in users:
        if user['userid'] ==u['userid']:
            return jsonify({'userdata':u,'requestcode': '200',
                        'message': 'login success'})
    return failedRequest('user doesnt exist')
@app.route('/getTruckById/', methods=['POST'])
def getTruckById():
    truck = json.loads(request.data)
    print(truck)
    for u in trucks:
        if truck['truckid'] ==u['truckid']:
            return jsonify({'userdata':u,'requestcode': '200',
                        'message': 'login success'})
    return failedRequest('truck doesnt exist')

@app.route('/getDriverById/', methods=['POST'])
def getDriverById():
    driver = json.loads(request.data)
    print(driver)
    for u in drivers:
        if drivers['userrolesid'] ==u['userrolesid']:
            return jsonify({'userdata':u,'requestcode': '200',
                        'message': 'login success'})
    return failedRequest('driver doesnt exist')


@app.route('/deleteUser/', methods=['POST'])
def deleteUser():
    user = json.loads(request.data)
    print(user)
    for u in users:
        if user['userid'] ==u['userid']:
           temp=u
    users.remove(temp)
    return jsonify({'requestcode': '200',
                        'message': 'success delete user'})

app.route('/deleteDriver/', methods=['POST'])
def deleteDriver():
    driver = json.loads(request.data)
    print(driver)
    for u in drivers:
        if drivers['userrolesid'] ==u['userrolesid']:
           temp=u
    drivers.remove(temp)
    return jsonify({'requestcode': '200',
                        'message': 'success delete driver'})

app.route('/deleteTruck/', methods=['POST'])
def deleteTruck():
    truck = json.loads(request.data)
    print(truck)
    for u in trucks:
        if trucks['truckid'] ==u['truckid']:
           temp=u
    users.remove(temp)
    return jsonify({'requestcode': '200',
                        'message': 'success delete Truck'})

app.route('/deletePort/', methods=['POST'])
def deletePort():
    port = json.loads(request.data)
    print(port)
    for u in ports:
        if ports['port_id'] ==u['port_id']:
           temp=u
    ports.remove(temp)
    return jsonify({'requestcode': '200',
                        'message': 'success delete port'}) 

@app.route('/loginAsDriver/', methods=['POST'])
def loginAsDriver():
    driver = json.loads(request.data)
    print(driver)
    for u in drivers:
        if driver['email'] ==u['email'] and driver['password']==u['password']:
            return jsonify({'requestcode': '200',
                        'message': 'login success',
                        'respBody':u})
    return failedRequest('driver doesnt exist')                        

@app.route('/sendMessageToMQ/', methods=['POST'])
def semdMessageToMq():
    message = json.loads(request.data)
    print(message)
    stringmessage=json.dumps(message)
    print(stringmessage)
    mttqpublis.run(stringmessage)      
    return jsonify({'requestcode': '200',
                        'message': 'success send message'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)